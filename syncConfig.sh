checkUpdates = (
  'check modify date',
  'compare modify date',
  'copy blindly (expensive)',
  'compare hashes'
)

triggerCheck = (
  'periodically',
  'on stratup/shutdown',
  'on demand, push all (expensive)'
)

source ./addAppConfig.sh "changed-app-path" "branch name"
