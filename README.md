# index

Except for this one, each branch represents a configuration version of<br>
a certain app which the name of the branch indicates, examples: `polybar-basic` and `ranger-exhaustive`.

Each branch is a different method/philosophy of using the app.

You can clone the config instance you want using the `--branch` option:

```bash
git clone https://gitlab.com/anasouardini/configshop --branch [branch name]
```

Here is an example for installing i3-wm configuration:

```bash
mkdir -p ~/tmp \
cd ~/tmp \
git clone https://gitlab.com/anasouardini/configshop --branch i3-only-tiling \
mkdir -p ~/.config/i3 \
cp ./configshop/config ~/.config/i3/
```

You can swap "i3-only-tiling" with another branch as you wish.

Here is a list of branches available:

- File Managers:
    - Ranger
        - [Exhaustive](https://gitlab.com/anasouardini/configshop/-/tree/ranger-exhaustive)

- Window Tiling Manager:
    - i3-WM
        - [only-tiling](https://gitlab.com/anasouardini/configshop/-/tree/i3-only-tiling)
    
- Status Bar
    - Polybar
        - [basic](https://gitlab.com/anasouardini/configshop/-/tree/polybar-basic)

## TODO:
- [ ] write a script that takes a list of app/repositor names and their specific branches<br>
      and clones them to their desired locations.
- [ ] a local environment would make adding apps or branches a lot easier
    - [ ] the local env could auto generate the generic installation script with all variables
- [ ] a script that updates or downgrades each or all apps.
