#!/bin/bash

appConfigPath=$1
branchName=$2

# todo: check if vars are provided correctly

echo "creating branch ${branchName}"
git branch $branchName
git switch $branchName

echo "copying config files over to the branch"
ls | grep -xv ".git" | xargs rm -rf
cp -r "$appConfigPath"/* ./

echo "deploying changes to origin"
git add . && git commit -m "auto commit: added config file(s)"
git push -f origin $branchName
